package com.carga_planillon.common;

import java.io.File;

public class FileHelpers {
	
	public FileHelpers() {
		
	}
	
	public static String getExtension(File verifiedSignature) {
		String fileName = verifiedSignature.getName();
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
		    extension = fileName.substring(i+1);
		}
		
		return extension;
	}
}
