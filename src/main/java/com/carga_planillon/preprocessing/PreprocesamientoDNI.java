package com.carga_planillon.preprocessing;

import java.util.List;

import com.carga_planillon.hough.LinearHT.HoughLine;

import ij.ImagePlus;
import ij.gui.Roi;
import ij.process.ImageProcessor;

public class PreprocesamientoDNI extends Preprocesamiento {

//    private static final int NUMERO_LINEAS_TRANSFORMADA_HOUGH = 2;
//    private static final int MIN_NUMERO_PUNTOS_POR_LINEA = 750;

    public ImagePlus getDNIConPreProceso(PreprocesamientoDNI preDNI,
            ImagePlus dniSinPreProcesar, ImagePlus dniACortar,
            int numLineasTransHough, int minNumPuntosPorLinea) {

        // Transformada de Hough
        List<HoughLine> lineas = preDNI.transformadaHough(dniSinPreProcesar,
                numLineasTransHough, minNumPuntosPorLinea);

        int yCorte = PluginHoughLinear.getCordenadaYCorte(lineas, dniSinPreProcesar, CorteY.INFERIOR) - 2;

        if (lineas.size() == 1) {
            yCorte = yCorte - 5;
        }


        Roi roi = new Roi(0, 0, dniACortar.getProcessor().getWidth(),
                yCorte);

        ImageProcessor dniACortarProce = dniACortar.getProcessor();
        dniACortarProce.setRoi(roi);
        ImagePlus newIma = new ImagePlus("dniRecortado", dniACortarProce.crop());

        return newIma;
    }

}
