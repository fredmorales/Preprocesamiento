package com.carga_planillon.preprocessing;

import java.awt.geom.Line2D;

public class Recta {


    public static int calculaYEnRectaConX ( Line2D.Double lin2D, int x2) {

        int y2 = 0;
        int u1 = (int) Math.rint(lin2D.x1);
        int v1 = (int) Math.rint(lin2D.y1);
        int u2 = (int) Math.rint(lin2D.x2);
        int v2 = (int) Math.rint(lin2D.y2);

        if(u2 == u1){
            return y2;
        }else if( v2 == v1){
            return v1;
        }else {
            int m = (v2 - v1)/(u2-u1);
            y2 = m * (x2 - u1) + v1;
            return (int) Math.rint(y2);
        }
    }

    public static int calculaXEnRectaConY( Line2D.Double lin2D, int y2) {

        int x2 = 0;
        int u1 = (int) Math.rint(lin2D.x1);
        int v1 = (int) Math.rint(lin2D.y1);
        int u2 = (int) Math.rint(lin2D.x2);
        int v2 = (int) Math.rint(lin2D.y2);

        if(u2 == u1){
            return u1;
        }else if( v2 == v1){
            return x2;
        }else {
            int m = (v2 - v1)/(u2-u1);
            x2 = (y2 -v1)/m + u1;
            return (int) Math.rint(x2);
        }

    }

}
