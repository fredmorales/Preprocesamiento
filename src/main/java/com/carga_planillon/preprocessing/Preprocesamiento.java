package com.carga_planillon.preprocessing;

import java.util.List;

import com.carga_planillon.boundingbox.Select_Bounding_Box;
import com.carga_planillon.hough.LinearHT.HoughLine;
import com.carga_planillon.particle.ParticleAnalyzer;

import fiji.threshold.Auto_Threshold;
import ij.ImagePlus;
import ij.plugin.filter.RankFilters;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

public abstract class Preprocesamiento {

    public void convertir8bits(ImagePlus imagen){
        ImageConverter imc = new ImageConverter(imagen);
        imc.convertToGray8();
    }

    public void normalizar(ImagePlus imagen){
        ContrastEnhancer constraste = new ContrastEnhancer();
        constraste.setNormalize(true);
        constraste.normalizar(imagen);
    }

    public void umbralizacion(ImagePlus imagen){
        Auto_Threshold threshold = new Auto_Threshold();
        threshold.umbralizar(imagen);
    }

    public void filtroMaxima(int radio, ImageProcessor impro){
        RankFilters rfDni = new RankFilters();
        rfDni.rank(impro, radio, RankFilters.MAX);
    }

    public void filtroMediana(int radio, ImageProcessor impro){
        RankFilters rfDni = new RankFilters();
        rfDni.rank(impro, radio, RankFilters.MEDIAN);
    }

    public void buscarBordes(ImageProcessor imapro){
        imapro.findEdges();
    }

    public List<HoughLine> transformadaHough(ImagePlus implus, int numLineas, int numMinPuntosEnLinea){
        PluginHoughLinear phl = new PluginHoughLinear();

        phl.setup("", implus);
        phl.setMaxNumeroLineas(numLineas);
        phl.setMinPuntoEnLinea(numMinPuntosEnLinea);
        phl.run(implus.getProcessor());
        return phl.getLineas();
    }
    
    public void limitarImagenFirmaInvertida(ImagePlus imagen){
    	
        Select_Bounding_Box boundingBox = new Select_Bounding_Box();
        boundingBox.setup("autoautocrop", imagen);
        boundingBox.run(imagen.getProcessor());
		
        
		ParticleAnalyzer particleAnalyzer = new ParticleAnalyzer(
				ParticleAnalyzer.SHOW_MASKS + ParticleAnalyzer.IN_SITU_SHOW, 0, null, 90,
				Double.POSITIVE_INFINITY);

		particleAnalyzer.analyze(imagen);
		
		
        boundingBox.setup("autoautocrop", imagen);
        boundingBox.run(imagen.getProcessor());
        
    }

}
