package com.carga_planillon.signature;

import java.awt.image.BufferedImage;
import com.carga_planillon.preprocessing.PreprocesamientoFirma;

import ij.ImagePlus;
import ij.plugin.filter.Binary;
import ij.process.ImageProcessor;

public class PreprocessingSignature {

	private final ImagePlus firmaSinPreProcesar;
	
	public PreprocessingSignature(String name, BufferedImage image) {
		this.firmaSinPreProcesar = new ImagePlus(name, image);
	}
	
	public BufferedImage getSkeletonizedImage() {
		ImagePlus imagenEsquele = obtenerImagenEsqueletizada(firmaSinPreProcesar.getProcessor().convertToByte(true));
		return imagenEsquele.getBufferedImage();
	}

	private ImagePlus obtenerImagenEsqueletizada(final ImageProcessor imp) {
		PreprocesamientoFirma preFirma = new PreprocesamientoFirma();

		// Esqueletizacion
		ImagePlus imagenEsquele = new ImagePlus("Img Esqueletizacion", imp.duplicate());

		// Filtro de la maxima
		final int RADIO_FILTRO_MAXIMA = 2;
		preFirma.filtroMaxima(RADIO_FILTRO_MAXIMA, imagenEsquele.getProcessor());

		Binary esqueleBinary = new Binary();
		esqueleBinary.setup("skel", imagenEsquele);
		esqueleBinary.run(imagenEsquele.getProcessor());

		imagenEsquele.getProcessor().invertLut();

		return imagenEsquele;
	}
}
